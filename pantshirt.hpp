
#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

typedef std::vector<std::pair<int, int> > valueVector;

class Pantscalculator
{
private:
  int _pantsValue;
  int _shirtValue;
  valueVector _solutions;

public:
  Pantscalculator();
  ~Pantscalculator();
  void	      computeSolutions();
  bool	      distinctDigits(int nb1, int nb2);
  bool	      checkNbr(int nbr, std::string &digits);
  valueVector &getValues();
};
