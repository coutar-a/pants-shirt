
#include "pantshirt.hpp"

Pantscalculator::Pantscalculator() : _pantsValue(0), _shirtValue(2)
{
}

Pantscalculator::~Pantscalculator() {}

valueVector	&Pantscalculator::getValues() {return _solutions;}

void	Pantscalculator::computeSolutions()
{
  for (; _pantsValue < 1000 ; _pantsValue++)
    {
      for (; _shirtValue < 1000 ; _shirtValue++)
	{
	  if ((_shirtValue * _pantsValue - _shirtValue - _pantsValue - 359) == 0 &&
	      (_pantsValue > _shirtValue) && distinctDigits(_pantsValue, _shirtValue))
	    {
	      //distinctDigits(_pantsValue, _shirtValue);
	    _solutions.push_back(std::pair<int, int>(_pantsValue, _shirtValue));
	    }
	  /*
	  if ((_pantsValue > _shirtValue) &&
	      distinctDigits(_pantsValue, _shirtValue) &&
	      _pantsValue == ((_shirtValue + 359) / (_shirtValue - 1)))
	      _solutions.push_back(std::pair<int, int>(_pantsValue, _shirtValue));*/
	}
      _shirtValue = 2;
    }
}

bool	Pantscalculator::checkNbr(int nbr, std::string &digits)
{
  std::string str = std::to_string(nbr);
  bool ret = true;

  //std::cout << "test for " << nbr << " : " << std::endl;
  for( unsigned int i = 0 ; i != str.size() ; i++)
    {
      //std::cout << "digits : " << digits << std::endl;
      if (digits.size() < 4 && digits.find(str[i]) == std::string::npos)
	{
	  //std::cout << "appending " << str[i] << " to digits" << std::endl;
	  digits += str[i];
	}
      else if (digits.size() >= 4 || digits.find(str[i]) != std::string::npos)
	{
	  //std::cout << str[i] << " was found in digits or digits is too large" << std::endl;
	  ret = false;
	  return ret;
	}
    }
  //std::cout << "digits : " << digits << std::endl;
  return ret;
}

bool	Pantscalculator::distinctDigits(int nb1, int nb2)
{
  std::string digits = "";
  bool test1 = checkNbr(nb1, digits);
  bool test2 = checkNbr(nb2, digits);
  return (test1 && test2);
}

int main(void)
{
  Pantscalculator	test;
  valueVector		output;
  
  test.computeSolutions();

  output = test.getValues();

  for_each(output.begin(), output.end(), [] (std::pair<int,int> pair) {
      std::cout << "pants cost : " << pair.first << " and shirt costs : " << pair.second << std::endl;
	});
  return 0;
}
